package tenis;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author xtravni2
 */
public class TennisGameImpl implements TennisGame {

    private Player playerA = new Player(); 
    private Player playerB = new Player(); 
     
    private void playerScored(Player player) {
        if (player.getScore()==0) {
            player.setScore(15);
        }
        else 
        if (player.getScore()==15) {
            player.setScore(30);
        }
        else
        if (player.getScore()==30) {
            player.setScore(40);
        }
        
    }
    @Override
    public void playerAScored() {
        playerScored(playerA);
    }
    
    @Override
    public void playerBScored() {
        playerScored(playerB);
    }    
        
    @Override
    public int getPlayerAScore() {
        return playerA.getScore();  
    }

    @Override
    public int getPlayerBScore() {
        return playerB.getScore();  
    }
    
}
