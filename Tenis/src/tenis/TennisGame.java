package tenis;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author xtravni2
 */
public interface TennisGame {

    void playerAScored();

    void playerBScored();

    int getPlayerAScore();

    int getPlayerBScore();

}
